import pygame
from .states.game import GameState

WINDOW_SIZE = width, height = (640, 480)


class Application(object):
    def __init__(self):
        pygame.init()

        self.width, self.height = 640, 480

        pygame.display.set_mode((self.width, self.height))
        self.state = GameState(self)

    def run(self):
        clock = pygame.time.Clock()
        while not self.state.done:
            ticks = clock.tick(60)
            self.state.tick(ticks)

    def destroy(self):
        pass
