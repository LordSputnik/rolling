class BaseState(object):

    def __init__(self, application):
        self.app = application
        self._done = False

    def tick(self, delta):
        raise NotImplementedError

    @property
    def done(self):
        return self._done
