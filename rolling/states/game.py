import pygame
from . import BaseState

speed = [2, 2]


class GameState(BaseState):
    def __init__(self, *args, **kwargs):
        self.ball = pygame.image.load("assets/ball.bmp")
        self.ballrect = self.ball.get_rect()

        super(GameState, self).__init__(*args, **kwargs)

    def tick(self, delta):
        screen = pygame.display.get_surface()
        delta_s = delta / 1000.0

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._done = True

        self.ballrect = self.ballrect.move(speed * delta_s)
        if self.ballrect.left < 0 or self.ballrect.right > self.app.width:
            speed[0] = -speed[0]
        if self.ballrect.top < 0 or self.ballrect.bottom > self.app.height:
            speed[1] = -speed[1]

        screen.fill((255, 255, 255))
        screen.blit(self.ball, self.ballrect)
        pygame.display.flip()
