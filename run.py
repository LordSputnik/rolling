#!/usr/bin/env python3

import rolling.application

app = rolling.application.Application()

app.run()

app.destroy()
